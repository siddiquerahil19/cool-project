import resumeBuilderProject from "../../assets/images/resumeBuilderProject.png";
export const workSectionInfo = [
  {
    img: resumeBuilderProject,
    description: "A simple website for creating resume",
    title: "Resume Builder",
    links: [
      "https://github.com/Rahilsiddique/resume-builder",
      "http://resume-builder-two-xi.vercel.app/"
    ]
  },
  {
    img: resumeBuilderProject,
    description: "Online coding assesment",
    title: "codeAck",
    links: [
      "https://github.com/Rahilsiddique/resume-builder",
      "http://resume-builder-two-xi.vercel.app/"
    ]
  },
  {
    img: resumeBuilderProject,
    description: "get latest news in seconds",
    title: "inshorts clone",
    links: [
      "https://github.com/Rahilsiddique/resume-builder",
      "http://resume-builder-two-xi.vercel.app/"
    ]
  }
];
