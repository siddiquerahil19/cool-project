import { projects } from "./projects";
import "./App.css";
import axios from "axios";
import { useEffect, useState } from "react";
import { objectExpression } from "@babel/types";

function App() {
  let identifyingLabel = "hacktoberfest-accepted";

  interface LeaderboardType {
    username: string;
    score: number;
  }

  let [leaderboard, setLeaderboard] = useState([] as LeaderboardType[]);

  let [users, setUsers]: any = useState([]);

  let [storeOnce, setStoreOnce]: any = useState({});

  useEffect(() => {
    async function fetch() {
      console.log("fetching");
      for (let m = 0; m < projects.length; m++) {
        let project = projects[m];

        await axios
          .get(
            `https://api.github.com/repos/${project.user}/${project.name}/pulls?state=closed`
          )
          .then(async function (response) {
            if (response.data && response.data.length > 0) {
              let prs = response.data;
              for (let i = 0; i < prs.length; i++) {
                let pr = prs[i];
                if (pr.labels && pr.labels.length > 0) {
                  let labels = pr.labels;
                  for (let j = 0; j < labels.length; j++) {
                    let label = labels[j];
                    if (label.name === identifyingLabel) {
                      let user = pr.user.login;
                      setUsers((users: any) => [...users, user]);
                      break;
                    }
                  }
                }
              }
            }
          });
      }
    }
    fetch();
  }, []);

  const result = users.reduce((prev: any, user: any) => {
    prev[user] = (prev[user] || 0) + 1;
    return prev;
  }, {});

  let temp = { ...result };
  return (
    <div className="App">
      {/* <header className="App-header"></header> */}
      <div>
        <h1>aasdas</h1>
        {Object.keys(temp).map((key) => {
          return (
            <div>
              {key} : {temp[key]}
            </div>
          );
        })}
      </div>
    </div>
  );
}

export default App;
