<h1 align="center">Resume Builder - ✌</h1>

### An app developed for creating a resume in few steps. <br> This app currently has only one format, but we have much bigger plans for it in the future. 


## Roadmap
- [x] App creation
- [x] (almost) Fixing problems
- [ ] Adding more compatibility
- [ ] Adding more formats
- [ ] ???

## How to use
- The *left field* is for entering data, and *right field* is for preview.
- You can use the project from this link - [Resume Builder](https://resume-builder-two-xi.vercel.app/)

## Built with
- **React.js** <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/React-icon.svg/2300px-React-icon.svg.png" height=20 align="center"/>

## Contributing

Contributions are what make the open source community such an amazing place to learn, inspire, and create. Any contributions you make are **greatly appreciated**.

If you have a suggestion that would make this better, please fork the repo and create a pull request. You can also simply open an issue with the tag "enhancement".
Don't forget to give the project a star! Thanks again!

## Author
- [Rahil](https://github.com/Rahilsiddique)


